module.exports = {
  pluginOptions: {
    runtimeCompiler: true,
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  },
  devServer: {
      disableHostCheck: true,
  }
};
