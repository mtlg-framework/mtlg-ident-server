import axios from "axios";

const host = window.location.hostname;
const protocol = window.location.protocol;
let port = window.location.port;

if (port == "") {
  port = "/ident"
} else {
  port = ":8002"
}

const axiosService = axios.create({
  headers: {
    "X-Req-With": "FRONTEND"
  },
  baseURL: protocol+ "//" + host + port
});

const token = localStorage.getItem("user-token");

if (token) {
  axiosService.defaults.headers.common["Authorization"] = token;
}

export default axiosService;
