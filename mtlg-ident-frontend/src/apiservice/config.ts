import apiservice from "./apiservice";

export default {
  get(gameId) {
    return apiservice.get(`/config/${gameId}`).then(({ data }) => {
      data.__fetched = Date.now();
      return data;
    });
  },
  put(gameId, config) {
    return apiservice.put(`/config?game_uuid=${gameId}`, config).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data
      };
    });
  },
  getStandard() {
    return apiservice.get(`/config/`).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data
      };
    });
  },
  putStandard(config) {
    return apiservice.put(`/config/standard`, config).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data
      };
    });
  },
};
