import apiservice from "./apiservice";

export default {
  get() {
    return apiservice.get("/manage/players/full/").then(({ data }) => {
      data.payload.forEach(function(p) {
        p.__fetched = Date.now();
      });
      return data.payload;
    });
  },
  getPlayer(pseudonym) {
    return apiservice.get(`/manage/players/${pseudonym}`).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data.payload
      };
    });
  },
  putPlayer(player) {
    return apiservice.put(`manage/player/`, player).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data.payload
      };
    });
  },
  postPlayers(players, lang = 'de') {
    const sendObj = {
      players: players,
      language: lang
    }
    return apiservice.post(`manage/players/`, sendObj).then(({ data }) => {
      data.players.forEach(function(p) {
        p.__fetched = Date.now();
      });
      return data.players;
    });
  },
  deletePlayer(pseudonym) {
    return apiservice.delete(`/manage/player/${pseudonym}`).then(({ data }) => {
      return data.payload;
    });
  }
};
