import apiservice from "./apiservice";

export default {
  get() {
    return apiservice.get("/games/").then(({ data }) => {
      data.payload.forEach(function(p) {
        p.__fetched = Date.now();
      });
      return data.payload;
    });
  },
  getGame(gameId) {
    return apiservice.get(`/game/${gameId}`).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data.payload
      };
    });
  },
  putGame(game) {
    return apiservice.put(`game/`, game).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data.payload
      };
    });
  },
  deleteGame(gameId) {
    return apiservice.delete(`/game/${gameId}`).then(({ data }) => {
      return data.payload;
    });
  }
};
