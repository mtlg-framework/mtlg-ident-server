import apiservice from "./apiservice";

export default {
  get() {
    return apiservice.get("/login_method/").then(({ data }) => {
      data.login_methods.forEach(function(p) {
        p.__fetched = Date.now();
      });
      return data.login_methods;
    });
  },
  getLoginMethod(uuid) {
    return apiservice.get(`login_method/${uuid}`).then(({ data }) => {
      data.payload.__fetched = Date.now();
      return data.payload;
    });
  },
  post(loginMethod) {
    return apiservice.post(`login_method/`, loginMethod).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data
      };
    });
  },
  put(loginMethod) {
    return apiservice.put(`login_method/${loginMethod.uuid}`, loginMethod).then(({ data }) => {
      return {
        __fetched: Date.now(),
        ...data.payload
      };
    });
  },
  delete(uuid) {
    return apiservice.delete(`/login_method/${uuid}`).then(({ data }) => {
      return data.payload;
    });
  }
};
