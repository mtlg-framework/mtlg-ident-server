import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import i18n from "../i18n";
import store from "../store";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Login from "../views/Login.vue";
import Games from "../views/games/Games.vue";
import GameEdit from "../views/games/GameEdit.vue";
import GameDetail from "../views/games/GameDetail.vue";
import GameNew from "../views/games/GameNew.vue";
import LoginMethods from "../views/loginMethods/LoginMethods.vue";
import LoginMethodsEdit from "../views/loginMethods/LoginMethodsEdit.vue";
import Players from "../views/players/Players.vue";
import PlayerNew from "../views/players/PlayerNew.vue";
import PlayersNew from "../views/players/PlayersNew.vue";
import PlayerDetail from "../views/players/PlayerDetail.vue";
import PlayerEdit from "../views/players/PlayerEdit.vue";
import StandardConfig from "../views/games/StandardConfig.vue"

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    component: About
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/games",
    name: "Games",
    component: Games
  },
  {
    path: "/games/add",
    name: "GameNew",
    component: GameNew
  },
  {
    path: "/games/:gameId",
    name: "GameDetail",
    component: GameDetail
  },
  {
    path: "/games/:gameId/edit",
    name: "GameEdit",
    component: GameEdit
  },
  {
    path: "/config",
    name: "StandardConfig",
    component: StandardConfig
  },
  {
    path: "/loginMethods",
    name: "LoginMethods",
    component: LoginMethods
  },
  {
    path: "/loginMethods/:loginMethodId/edit",
    name: "LoginMethodsEdit",
    component: LoginMethodsEdit
  },
  {
    path: "/players/",
    name: "Players",
    component: Players
  },
  {
    path: "/players/add",
    name: "PlayerNew",
    component: PlayerNew
  },
  {
    path: "/players/add/multiple",
    name: "PlayersNew",
    component: PlayersNew
  },
  {
    path: "/players/:pseudonym/",
    name: "PlayerDetail",
    component: PlayerDetail
  },
  {
    path: "/players/:pseudonym/edit",
    name: "PlayerEdit",
    component: PlayerEdit
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

const checkAuth = function(to, from, next) {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/login", "/register"];
  const authRequired = !publicPages.includes(to.path);

  if (publicPages.includes(to.path)) {
    // no auth required
    next();
  } else {
    if (store.state.misc.loggedIn) {
      // user is already active and logged in
      next();
    } else {
      if (localStorage.getItem("user-token")) {
        // localestorage present, check for validity
        store
          .dispatch("misc/GET_CURRENT_USER")
          .then(() => {
            next();
          })
          .catch(() => {
            console.err("Got loginstatus, but cannot auth successfully");
          });
      } else {
        next("/login");
      }
    }
  }
};

router.beforeEach((to, from, next) => {
  if (
    store.state.language.language &&
    store.state.language.language !== i18n.locale
  ) {
    i18n.locale = store.state.language.language;
    checkAuth(to, from, next);
  } else if (!store.state.language.language) {
    store.dispatch("language/setLanguage", navigator.languages).then(() => {
      i18n.locale = store.state.language.language;
      checkAuth(to, from, next);
    });
  } else {
    checkAuth(to, from, next);
  }
});

export default router;
