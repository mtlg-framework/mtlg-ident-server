export default {
  menu: {
    home: "Home",
    about: "About"
  },
  main: {
    welcome: "Hello from {company}!"
  },
  languagePickerHelper: "Select language",
  languages: {
    en: "English",
    de: "German"
  }
};
