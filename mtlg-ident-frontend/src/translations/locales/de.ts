export default {
  menu: {
    home: "Start",
    about: "Über"
  },
  main: {
    welcome: "Hallo von {company}!"
  },
  languagePickerHelper: "Sprache wählen",
  languages: {
    en: "Englisch",
    de: "Deutsch"
  }
};
