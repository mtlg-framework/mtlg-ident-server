import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import i18n from "./i18n";
Vue.config.productionTip = false;

Vue.prototype.app_config = {
  _cache_timeout: 30*1000 // 30s
};

import moment from 'moment'

Vue.filter('formatDate', function(value: any) {
  if (value) {
    return moment(String(value)).format('DD.MM.YYYY HH:mm')
  }
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
