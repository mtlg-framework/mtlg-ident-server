import apiservice from "@/apiservice/apiservice";

export default {
  namespaced: true,

  state: {
    currentUser: {},
    loggedIn: false
  },

  mutations: {
    SET_CURRENT_USER(state, user) {
      state.currentUser = user;
      state.loggedIn = true;
    },
    REMOVE_CURRENT_USER(state) {
      state.currentUser = {};
      state.loggedIn = false;
    }
  },

  actions: {
    GET_CURRENT_USER(context) {
      return apiservice
        .get("/admin/current_user")
        .then(({ data }) => {
          context.commit("SET_CURRENT_USER", data.payload);
        })
        .catch(error => {
          console.log(error);
        })
        .finally(() => {
          this.loading = false;
        });
    }
  }
};
