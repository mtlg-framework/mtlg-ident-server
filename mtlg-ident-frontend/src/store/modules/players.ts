import players from "@/apiservice/players";
import Vue from "vue";

export default {
  namespaced: true,

  state: {
    players: [],
    player: {}
  },

  mutations: {
    SET_PLAYERS(state, players) {
      state.players = players;
    },
    SET_PLAYERS_ADD(state, players) {
      state.players.concat(players);
    },
    SET_PLAYER(state, player) {
      // test if game is already in games
      let inserted = false;
      state.players.forEach(function(e, i, a) {
        if (e.uuid === player.uuid) {
          Vue.set(a, i, player); // reactivity
          inserted = true;
        }
      });
      if (!inserted) {
        state.players.push(player);
      }
      state.player = player;
    },
    DELETE_PLAYER(state, pseudonym) {
      state.players.forEach(function(e, i, a) {
        if (e.pseudonym === pseudonym) {
          state.players.splice(i, 1);
        }
      });
    }
  },

  actions: {
    GET_PLAYERS(context) {
      return players
        .get()
        .then(payload => {
          context.commit("SET_PLAYERS", payload);
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    },
    GET_PLAYER(context, pseudonym) {
      return players
        .getPlayer(pseudonym)
        .then(payload => {
          context.commit("SET_PLAYER", payload)
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    },
    PUT_PLAYER(context, player) {
      return players
        .putPlayer(player)
        .then(payload => {
            context.commit("SET_PLAYER", player);
        })
        .catch(error => {
          return Promise.reject(error);
        })
        .finally(error => {
          this.loading = false;
        });
    },
    POST_PLAYERS(context, playersArr, lang = 'de') {
      return players
          .postPlayers(playersArr, lang)
          .then(payload => {
            context.commit("SET_PLAYERS_ADD", payload);
          })
          .catch(error => console.log(error))
          .finally(() => {
            this.loading = false;
          });
    },
    DESTROY_PLAYER(context, pseudonym) {
      return players
        .deletePlayer(pseudonym)
        .then(payload => {
          context.commit('DELETE_PLAYER', pseudonym)
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    }
  }
};
