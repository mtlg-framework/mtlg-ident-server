import games from "@/apiservice/games";
import config from "@/apiservice/config";
import Vue from "vue";

export default {
  namespaced: true,

  state: {
    games: [],
    game: {
      config: {
        login_methods: [],
        listable_names: false,
        listable_pseudonyms: false
      }
    }
  },

  mutations: {
    SET_GAMES(state, games) {
      state.games = games;
    },
    SET_GAME(state, game) {
      // test if game is already in games
      let inserted = false;
      state.games.forEach(function(e, i, a) {
        if (e.uuid === game.uuid) {
          Vue.set(a, i, game); // reactivity
          inserted = true;
        }
      });
      if (!inserted) {
        state.games.push(game);
      }
      if (!game.config) {
        game.config = {
          login_methods: [],
          listable_names: false,
          listable_pseudonyms: false
        }
      }
      state.game = game;
    },
    DELETE_GAME(state, gameId) {
      state.games.forEach(function(e, i, a) {
        if (e.uuid === gameId) {
          state.games.splice(i, 1);
        }
      });
    }
  },

  actions: {
    GET_GAMES(context) {
      return games
        .get()
        .then(payload => {
          context.commit("SET_GAMES", payload);
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    },
    GET_GAME(context, gameId) {
      return games
        .getGame(gameId)
        .then(payload => {
          return config.get(gameId).then(configPayload => {
            payload.players.forEach(function(p) {
              p.__fetched = Date.now();
            });
            payload.config = configPayload;
            context.commit("SET_GAME", payload);
            // context.commit("players/SET_PLAYERS", payload.players, {
            //   root: true
            // });
          })
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    },
    GET_CONFIG(context, game) {
      return config.get(game.uuid).then(config_data => {
        game.config = config_data;
      }).catch((err) => {
        game.config = {
          login_methods: [],
          listable_names: false,
          listable_pseudonyms: false
        }
      }).finally(() => {
        context.commit("SET_GAME", game);
      })
    },
    PUT_GAME(context, game) {
      return games
        .putGame(game)
        .then(payload => {
          config.put(payload.uuid, game.config).then(config => {
            payload.config = config;
            context.commit("SET_GAME", payload);
          })
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    }, DESTROY_GAME(context, gameId) {
      return games
        .deleteGame(gameId)
        .then(payload => {
          context.commit("DELETE_GAME", gameId);
        })
        .catch(error => console.log(error))
        .finally(() => {
          this.loading = false;
        });
    }
  }
};
