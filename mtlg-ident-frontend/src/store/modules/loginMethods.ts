import Vue from "vue";
import loginMethods from "@/apiservice/loginMethods";

export default {
    namespaced: true,

    state: {
        loginMethods: [],
        loginMethod: {}
    },

    mutations: {
        SET_LOGIN_METHODS(state, methods) {
            state.loginMethods = methods;
        },
        SET_LOGIN_METHOD(state, loginMethod) {
            let inserted = false;
            state.loginMethods.forEach(function (e, i, a) {
                if (e.uuid === loginMethod.uuid) {
                    Vue.set(a, i, loginMethod); // reactivity
                    inserted = true;
                }
            });
            if (!inserted) {
                state.loginMethods.push(loginMethod);
            }
            state.loginMethod = loginMethod;
        },
        DELETE_LOGIN_METHOD(state, uuid) {
            state.loginMethods.forEach(function (e, i, a) {
                if (e.uuid === uuid) {
                    state.loginMethods.splice(i, 1);
                }
            });
        }
    },

    actions: {
        GET_LOGIN_METHODS(context) {
            return loginMethods
                .get()
                .then(payload => {
                    context.commit("SET_LOGIN_METHODS", payload);
                })
                .catch(error => console.log(error))
                .finally(() => {
                    this.loading = false;
                });
        },
        GET_LOGIN_METHOD(context, uuid) {
            return loginMethods
                .getLoginMethod(uuid)
                .then(payload => {
                    context.commit("SET_LOGIN_METHODS", payload);
                })
                .catch(error => console.log(error))
                .finally(() => {
                    this.loading = false;
                });
        },
        POST_LOGIN_METHOD(context, loginMethod) {
            return loginMethods
                .post(loginMethod)
                .then(payload => {
                    context.commit("SET_LOGIN_METHOD", payload);
                })
                .catch(error => console.log(error))
                .finally(() => {
                    this.loading = false;
                });
        },
        PUT_LOGIN_METHOD(context, loginMethod) {
            return loginMethods
                .put(loginMethod)
                .then(payload => {
                    context.commit("SET_LOGIN_METHOD", loginMethod);
                })
                .catch(error => console.log(error))
                .finally(() => {
                    this.loading = false;
                });
        },
        DESTROY_LOGIN_METHOD(context, uuid) {
            return loginMethods
                .delete(uuid)
                .then(payload => {
                    context.commit("DELETE_LOGIN_METHOD", uuid);
                })
                .catch(error => console.log(error))
                .finally(() => {
                    this.loading = false;
                });
        }
    }
};
