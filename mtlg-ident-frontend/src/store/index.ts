import Vue from "vue";
import Vuex from "vuex";

//Modules
import language from "./modules/language";
import misc from "./modules/misc";
import games from "./modules/games";
import players from "./modules/players";
import loginMethods from "./modules/loginMethods";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    language,
    games,
    misc,
    players,
    loginMethods
  }
});

export default store;
