from mongoengine import StringField, EmailField, ListField, ReferenceField
from werkzeug.security import generate_password_hash, check_password_hash

from .base import Base


class User(Base):
    username = StringField(max_length=200, required=True, unique=True)
    email = EmailField(max_length=200, required=True)
    password_hash = StringField(max_length=200, required=True)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


    def as_dict(self, full=False):
        data = {
            'instance': 'user',
            'username': self.username,
            'password_hash': self.password_hash,
            'email': self.email,
            **super(User, self).as_dict()
        }
        return data
