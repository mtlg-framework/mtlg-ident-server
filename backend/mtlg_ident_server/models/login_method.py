from mongoengine import StringField

from .base import Base

class LoginMethod(Base):
    name = StringField(max_length=200, required=True)
    description = StringField()

    def as_dict(self):
        return {
            '_instance': 'login_method',
            'name': self.name,
            'description': self.description,
            **super(LoginMethod, self).as_dict()
        }
