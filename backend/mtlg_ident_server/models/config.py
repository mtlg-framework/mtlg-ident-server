from mongoengine import ReferenceField, StringField, ListField, BooleanField

from .base import Base
from .game import Game

class StandardGameConfig(Base):
    login_methods= ListField(StringField())
    listable_names= BooleanField(default=True)
    listable_pseudonyms= BooleanField(default=True)

    def as_dict(self):
        return {
            '_instance': 'config',
            'login_methods': self.login_methods,
            'listable_pseudonyms': self.listable_pseudonyms,
            'listable_names': self.listable_names,
            **super(StandardGameConfig, self).as_dict()
        }

    def save(self, *args, **kwargs):
        if not self.login_methods:
            self.login_methods = []
        return super(StandardGameConfig, self).save(*args, **kwargs)


class GameConfig(Base):
    login_methods= ListField(StringField())
    listable_names= BooleanField(default=True)
    listable_pseudonyms= BooleanField(default=True)
    game= ReferenceField(Game)

    def as_dict(self):
        return {
            '_instance': 'config',
            'login_methods': self.login_methods,
            'listable_pseudonyms': self.listable_pseudonyms,
            'listable_names': self.listable_names,
            'game': self.game.as_dict(),
            **super(GameConfig, self).as_dict()
        }

    def save(self, *args, **kwargs):
        if not self.login_methods:
            self.login_methods = []

        return super(GameConfig, self).save(*args, **kwargs)
