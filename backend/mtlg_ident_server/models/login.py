from mongoengine import StringField, ReferenceField

from .base import Base
from .player import Player
from .login_method import LoginMethod

class Login(Base):
    login_method = ReferenceField(LoginMethod)
    login_value = StringField()
    player = ReferenceField(Player)

    def as_dict(self):
        return {
            '_instance': 'login',
            'login_method': self.login_method,
            'login_value': self.login_value,
            'player': self.player.as_dict(),
            'forename': self.forename,
            'surname': self.surname,
            **super(Player, self).as_dict()
        }

    def as_export(self):
        return {
            'method': self.login_method,
            'value': self.login_value
        }
