import datetime
from uuid import uuid4

from mongoengine import Document, StringField, DateTimeField, EmailField, ListField, ReferenceField, UUIDField


class Base(Document):
    uuid = UUIDField(default=uuid4)
    created_at = DateTimeField(default=datetime.datetime.utcnow)
    modified_at = DateTimeField(default=None)
    deleted_at = DateTimeField(default=None)

    meta = {
        'abstract': True,
        'strict': False,
        'indexes': [
            {
                'fields': ['uuid'],
                'unique': True
            }
        ]
    }

    def save(self, *args, **kwargs):
        if self.id:
            self.modified_at = datetime.datetime.now()
        else:
            self.created_at = datetime.datetime.now()

        return super(Base, self).save(*args, **kwargs)

    def as_dict(self):
        return {
            'class': str(self.__class__),
            'created_at': str(self.created_at),
            'modified_at': str(self.modified_at),
            'deleted_at': str(self.deleted_at),
            'uuid': str(self.uuid),
            '_id': str(self.id)
        }

    def as_export(self):
        return {
            'created_at': str(self.created_at),
            'modified_at': str(self.modified_at),
            'deleted_at': str(self.deleted_at),
            'uuid': str(self.uuid),
            '_id': str(self.id)
        }
