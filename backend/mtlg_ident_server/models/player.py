from mongoengine import StringField

from .base import Base


class Player(Base):
    nickname = StringField(max_length=200, default="")
    pseudonym = StringField(required=True)
    forename = StringField(max_length=300, default="")
    surname = StringField(max_length=300, default="")

    def as_dict(self):
        return {
            '_instance': 'player',
            'nickname': self.nickname,
            'pseudonym': self.pseudonym,
            'forename': self.forename,
            'surname': self.surname,
            **super(Player, self).as_dict()
        }

    def as_export(self):
        return {
            'nickname': self.nickname,
            'pseudonym': self.pseudonym,
            'forename': self.forename,
            'surname': self.surname,
            **super(Player, self).as_export()
        }
