from mongoengine import StringField, ReferenceField, ListField

from .base import Base
from .player import Player

class Game(Base):
    name = StringField(max_length=200, required=True)
    description = StringField()

    players = ListField(ReferenceField(Player))

    def as_dict(self):
        return {
            '_instance': 'game',
            'name': self.name,
            'description': self.description,
            'players': [p.as_dict() for p in self.players],
            **super(Game, self).as_dict()
        }

    def as_export(self):
        return {
            'name': self.name,
            'description': self.description,
            'players': [p.as_export() for p in self.players],
            **super(Game, self).as_export()
        }
