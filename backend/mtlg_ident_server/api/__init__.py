from uuid import UUID

from fastapi import Depends
from fastapi.security import APIKeyCookie
from fastapi.security.api_key import APIKeyHeader
from mongoengine import DoesNotExist

from backend.mtlg_ident_server.api.rredis import redis_connection
from backend.mtlg_ident_server.models import User

# apikey_scheme = APIKeyCookie(name="token")
apikey_scheme = APIKeyHeader(name="Authorization")


def auth_required(token: str = Depends(apikey_scheme)): # = Depends(apikey_scheme)
    """
    Checks if user transmits authentication token, and checks it with redis.
    @param token: Auth-Token
    @return: UUID of user, or None if invalid.
    """
    user_uuid = redis_connection.get(f"login-{token}-uuid")

    if user_uuid:
        return user_uuid.decode("utf-8")

    return user_uuid


def get_current_user(user_uuid: UUID = Depends(auth_required)):
    """
    Pulls user corresponding to uuid of database.
    @param user_uuid:
    @return: User or None if invalid uuid.
    """
    try:
        user = User.objects.get(uuid=user_uuid)
    except DoesNotExist:
        return None
    return user
