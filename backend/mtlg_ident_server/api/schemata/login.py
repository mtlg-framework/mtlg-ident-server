from pydantic import BaseModel
from typing import Optional, List

from . import BaseAttributes


class CreatePlayerRequest(BaseModel):
    nickname: Optional[str]
    surname: Optional[str]
    forename: Optional[str]
    language: Optional[str] = 'de'
    uuid: Optional[str]
    pseudonym: Optional[str]

class PlayersName(BaseModel):
    surname: str = ''
    forename: str = ''
    nickname: str = ''
    login_method: Optional[str] = ''
    login_value: Optional[str] = ''

class CreatePlayersRequest(BaseModel):
    players: List[PlayersName]
    language: Optional[str] = 'de'

class CreateLoginRequest(BaseModel):
    nickname: str
    surname: Optional[str]
    forename: Optional[str]
    login_method: str
    login_value: str
    language: Optional[str] = 'de'

class PlayersNameResponse(BaseAttributes):
    surname: Optional[str]
    forename: Optional[str]
    nickname: Optional[str]
    pseudonym: str

    class Config:
        orm_mode = True

class PlayerResponse(BaseModel):
    nickname: Optional[str]
    pseudonym: str

class PlayerCreationResponse(BaseModel):
    players: List[PlayersNameResponse]

class PseudonymRequest(BaseModel):
    pseudonym: str

class MethodLoginRequest(BaseModel):
    nickname: str
    method: str
    value: str

class SetNicknameRequest(BaseModel):
    pseudonym: str
    newNick: str

class ManageLoginMethodRequest(BaseModel):
    method: str
    value: str
    pseudonym: str

class LoginMethodResponse(BaseAttributes):
    name: str
    description: str = ""

    class Config:
        orm_mode = True

class LoginMethodRequest(BaseModel):
    name: str
    description: str

class DeleteLoginRequest(BaseModel):
    method: str
    pseudonym: str

class CreateGameRequest(BaseModel):
    name: str
    description: str
    players: List[str]
    uuid: Optional[str]

class GameResponse(BaseAttributes):
    name: str
    description: str

    class Config:
        orm_mode = True

class GameFullResponse(BaseAttributes):
    name: str
    description: str
    players: List[PlayersNameResponse]

    class Config:
        orm_mode = True

class GamesResponse(BaseModel):
    games: List[GameResponse]

    class Config:
        orm_mode = True

class ErrorResponse(BaseModel):
    error: str

class SuccessResponse(BaseModel):
    success: bool = True

class AdminLoginRequest(BaseModel):
    username: str
    password: str


class ConfigResponse(BaseAttributes):
    login_methods: List[str]
    listable_names: bool
    listable_pseudonyms: bool

    class Config:
        orm_mode = True

class CreateConfigRequest(BaseModel):
    login_methods: List[str]
    listable_names: bool
    listable_pseudonyms: bool

class UserResponse(BaseAttributes):
    username: str
    email: str

    class Config:
        orm_mode = True
