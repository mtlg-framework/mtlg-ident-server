from datetime import datetime
from uuid import UUID
from pydantic import BaseModel
from pydantic.generics import GenericModel
from typing import TypeVar, Generic, Optional

DataT = TypeVar("DataT")

class BaseAttributes(BaseModel):
    uuid: UUID
    created_at: datetime
    modified_at: datetime = None
    deleted_at: datetime = None

    instance: str = 'undefined'

class FrontendResponse(GenericModel, Generic[DataT]):
    payload: Optional[DataT] = None
    status: Optional[str] = None
