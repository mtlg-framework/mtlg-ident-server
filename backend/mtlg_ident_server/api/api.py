from fastapi import APIRouter, Depends
from fastapi.responses import HTMLResponse
from backend.mtlg_ident_server.models import Player
from backend.mtlg_ident_server.api.endpoints import manage, login, admin, game, config, login_method
from backend.mtlg_ident_server.api import get_current_user, auth_required
from starlette.requests import Request

api_router = APIRouter()

api_router.include_router(manage.router, prefix="/manage")
api_router.include_router(admin.router, prefix="/admin")
api_router.include_router(login.router, prefix="/login")
api_router.include_router(login_method.router, prefix="/login_method")
api_router.include_router(config.router, prefix="/config")
api_router.include_router(game.router, dependencies=[Depends(auth_required)])

@api_router.get("/")
def index(request: Request):
    html_content = """
                       <html>
                           <head>
                               <title>Ident Server</title>
                           </head>
                           <body>
                           <span>
                               {body}
                           </span>
                           </body>
                           <style>
                           .code {{
                                margin: 10px;
                                border: solid 1px black;
                                width: 40rem;
                                padding: 10px;
                           }}
                           </style>
                       </html>
                       """.format(body=get_readme(request))
    return HTMLResponse(content=html_content, status_code=200)

def get_readme(request):
    host = request.url
    hoststr = "{host}".format(host=host)
    hostWithoutScheme = hoststr.replace(request.url.scheme, '')[3:]
    hostWithoutPort = hoststr[:-1]
    if request.url.port:
        hostWithoutPort = hoststr.replace(str(request.url.port), '')[:-2]
    readme = """
    <h1> Ident-Server </h1>
    The ident-server is to manage players.<br>
    Players with and without login credentials can be created as well as deleted.<br>
    Logins with credentials or pseudonyms can be verified.<br>
    The admin can also configure the games.<br>

    <h3> Using the Ident Server </h3>
    In order to send the request from you game to your ident server instead of the global one, you have to specify the url in the settings of you game.<br>
    You have to add the following code in `game.config.js`:<br>
    <pre>
    MTLG.loadOptions({{
        ...,
        login: {{
            host: "{hostWithoutScheme}" // NOTE: without http
        }},
        ...
    }})<
    </pre>

    <h3> Managing Ident Server </h3>
    The frontend for the ident server is out-of-the-box on port 8003. (So if you do not configure something else the you can find the frontend under <a href={hostWithoutPort}:8003>{hostWithoutPort}:8003</a>)<br>
    There you can manage games, login methods and players.<br>

    <h2> API </h2>
    On <a href={host}docs>{host}docs</a> you find an interactive overview of the API.<br>

    <h3> Manage players and login </h3>

    - <strong>GET /manage/players/:</strong><br><br>
        Returns all players nicknames as json:<br>
        <pre>
        {{
          "players": [
            "Nickname1",
            "Nickname2",
            ...
          ]
        }}
        </pre>
    - <strong>GET /manage/players/full/ :lock: :</strong><br>
        Returns a json with status and payload. The playload contains an array of all players with all information. You have to be authorized for this request.
        <pre>
          {{
            "payload": [
              {{
                "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "created_at": "2021-02-16T16:23:37.738Z",
                "modified_at": "2021-02-16T16:23:37.738Z",
                "deleted_at": "2021-02-16T16:23:37.738Z",
                "instance": "string",
                "surname": "string",
                "forename": "string",
                "nickname": "string",
                "pseudonym": "string"
              }}
            ],
            "status": "string"
          }}
        </pre>
    - <strong>GET /manage/players/{{pseudonym}} :lock: :</strong><br>
        `{{pseudonym}}` must be replaced with the pseudonym of the player you want to get. It returns the full information of the player.
        <pre>
            {{
              "payload": {{
                "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "created_at": "2021-02-16T16:29:26.471Z",
                "modified_at": "2021-02-16T16:29:26.471Z",
                "deleted_at": "2021-02-16T16:29:26.471Z",
                "instance": "string",
                "surname": "string",
                "forename": "string",
                "nickname": "string",
                "pseudonym": "string"
              }},
              "status": "string"
            }}
        </pre>
    - <strong>POST /manage/players/ :lock: :</strong><br>
        This request creates several new players. The request body can look like:
        <pre>
          {{
            "players": [
              {{
                "surname": "string",
                "forename": "string",
                "nickname": "string",
                "login_method": "string",
                "login_value": "string"
              }}
            ],
            "language": "string"
          }}
        </pre>
        In the players array you can provide player objects. None parameter in the player object is required but you have to give at least one.<br>
        The response is like the request body but contains the genereated pseudonym and meta info like uuid and dates.<br>
    - <strong>PUT /manage/player/:</strong><br>
        This request creates or updates a player. The request body can look like:
        <pre>
        {{
          "nickname": "string",
          "surname": "string",
          "forename": "string",
          "language": "string", // default: "de"
          "uuid": "string",
          "pseudonym": "string"
        }}
        </pre>
        None of the parameters is required. If you do not provide any of them, a pseudonym is requested and you can set nickname, etc at a later time.<br>
        The response looks like:
        <pre>
          {{
            "nickname": "string",
            "pseudonym": "string"
          }}
        </pre>
    - <strong>PUT /manage/login/:</strong><br>
        This request also creates a player but with a login method already. The request body can look like:
        <pre>
        {{
          "nickname": "string",
          "surname": "string",
          "forename": "string",
          "login_method": "string",
          "login_value": "string",
          "language": "string", // default: "de"
        }}
        </pre>
        `nickname`, `login_method` and `login_value` are required. `login_method` takes the uuid of an existing login method.<br>
        The successful response looks like:
        <pre>
          {{
            "nickname": "string",
            "pseudonym": "string"
          }}
        </pre>
    - <strong>PUT /manage/login/method/:</strong><br>
        This request creats or updates a login for the given pseudonym. The request body should look like
        <pre>
        {{
          "method": "string",
          "value": "string",
          "pseudonym": "string"
        }}
        </pre>
        where `method` the uuid of the desired login method is. The json response gives:
        <pre>
        {{
          "success": true
        }}
        </pre>
    - <strong>PUT /manage/setNickname/:</strong><br>
        This request sets the nickname of the given pseudonym. The request body must look like:
        <pre>
        {{
          "pseudonym": "string",
          "newNick": "string"
        }}
        </pre>
        The successful response looks like:
        <pre>
        {{
          "nickname": "string",
          "pseudonym": "string"
        }}
        </pre>
    - <strong>DELETE /manage/player/{{pseudonym}} :lock: :</strong><br>
        This request deletes the player with the given pseudonym. The json response looks like:
        <pre>
        {{
          "success": true
        }}
        </pre>
    - <strong>DELETE /manage/login/method/:</strong><br>
        This request deletes the login method of the player with the given pseudonym.<br>
        The request body should look like:
        <pre>
        {{
          "method": "string",
          "pseudonym": "string"
        }}
        </pre>
        `method` takes the uuid id of the login method that should be deleted.<br>
        The json response looks like:<br>
        <pre>
        {{
          "success": true
        }}
        </pre>

    <h3> Admin</h3
    - <strong>POST /admin/login/</strong><br>
        This request logs in the admin user if authorized. It takes the username and password<br>
    - <strong>DELETE /admin/login/</strong><br>
        This request logs out the logged in user.<br>
    - <strong>GET /admin/current_user :lock:</strong><br>
        Returns the currently logged in user. Only possible when authorized.

    <h3> Login </h3>
    - <strong>POST /login/pseudonym/:</strong><br>
        This request logs in a player with their pseudonym. And return nickname and pseudonym.
    - <strong>POST /login/method/:</strong><br>
        This request logs in a player with the given nickname, login method and value if correct.

    <h3> Creating Login methods (the methods not the credentials themselves) </h3>
    - <strong>GET /login_method/:</strong><br>
        This request returns all created login methods like:
        <pre>
        {{
          "login_methods": [
              {{
                "_instance": "login_method",
                "name": "Pin",
                "description": "string",
                "class": "string",
                "created_at": "2020-10-26 09:26:29.321000",
                "modified_at": "2020-10-26 13:11:29.903000",
                "deleted_at": "None",
                "uuid": "99b53d9c-4382-4bec-b79f-d24f1f4cee0c",
                "_id": "5f96964537870d293f88c14f"
              }}
          ]
        }}
        </pre>
    - <strong>POST /login_method/:</strong><br>
        Creates a login method. The request body should look like:
        <pre>
        {{
          "name": "string",
          "description": "string"
        }}
        </pre>
        Response looks like:
        <pre>
        {{
          "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "created_at": "2021-02-20T18:27:53.901Z",
          "modified_at": "2021-02-20T18:27:53.901Z",
          "deleted_at": "2021-02-20T18:27:53.901Z",
          "instance": "string",
          "name": "string",
          "description": "string"
        }}
        </pre>
    - <strong>GET /login_method/{{uuid}}:</strong><br>
        This request returns the specifed login method. The response looks like:
        <pre>
        {{
          "payload": {{
            "_instance": "login_method",
            "name": "Pin",
            "description": "string",
            "class": "string",
            "created_at": "2020-10-26 09:26:29.321000",
            "modified_at": "2020-10-26 13:11:29.903000",
            "deleted_at": "None",
            "uuid": "99b53d9c-4382-4bec-b79f-d24f1f4cee0c",
            "_id": "5f96964537870d293f88c14f"
          }}
        }}
        </pre>
    - <strong>PUT /login_method/{{uuid}}:</strong><br>
        This method updates the login method with the given uuid. The request body should look like:
        <pre>
        {{
        "name": "string",
        "description": "string"
        }}
        </pre>
        Response looks like:
        </pre>json
        {{
        "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "created_at": "2021-02-20T18:27:53.901Z",
        "modified_at": "2021-02-20T18:27:53.901Z",
        "deleted_at": "2021-02-20T18:27:53.901Z",
        "instance": "string",
        "name": "string",
        "description": "string"
        }}
        </pre>
    - <strong>DELETE /login_method/{{uuid}}:</strong><br>
        This request deletes the login method with the given uuid.<br>
        The json response looks like:
        <pre>
        {{
          "success": true
        }}
        </pre>

    <h3> Configuration of games </h3>
    - <strong>PUT /config/standard:</strong><br>
        This request creates or updates the standard config. The standard config is used for games where no config has been set.<br>
        The request body must look like:
        <pre>
        {{
          "login_methods": [
            "string"
          ],
          "listable_names": true,
          "listable_pseudonyms": true
        }}
        </pre>
        Here `login_methods` is an array of the names of the methods. `listable_names` indicates if the names of the players can be shown as list in the game. Same for `listable_pseudonyms`.
    - <strong>GET /config/{{game_uuid}}:</strong><br>
        This request returns the config of the given uuid of a game. Here the game uuid is a paremeter in the url.<br>
        The response looks like:
        <pre>
        {{
          "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "created_at": "2021-02-20T18:47:21.298Z",
          "modified_at": "2021-02-20T18:47:21.298Z",
          "deleted_at": "2021-02-20T18:47:21.298Z",
          "instance": "string",
          "login_methods": [
            "string"
          ],
          "listable_names": true,
          "listable_pseudonyms": true
        }}
        </pre>
    - <strong>GET /config/:</strong><br>
        Same as <strong>GET /config/{{game_uuid}}:</strong> but here the uuid of the game is given as query parameter

    - <strong>PUT /config/ :lock: :</strong><br>
        This request creats or updates the configuration for a given game.<br>
        The uuid of the game must be given as query parameter and the request body must look like:
        <pre>
        {{
          "login_methods": [
            "string"
          ],
          "listable_names": true,
          "listable_pseudonyms": true
        }}
        </pre>
        The response looks like:
        <pre>
        {{
          "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "created_at": "2021-02-20T18:49:38.338Z",
          "modified_at": "2021-02-20T18:49:38.338Z",
          "deleted_at": "2021-02-20T18:49:38.338Z",
          "instance": "string",
          "login_methods": [
            "string"
          ],
          "listable_names": true,
          "listable_pseudonyms": true
        }}
        </pre>

    <h3> Games </h3>
    - <strong>GET /games/ :lock: :</strong><br>
        This request returns all created games. The response looks like:
        <pre>
        {{
          "payload": [
            {{
              "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
              "created_at": "2021-02-20T18:50:15.543Z",
              "modified_at": "2021-02-20T18:50:15.543Z",
              "deleted_at": "2021-02-20T18:50:15.543Z",
              "instance": "string",
              "name": "string",
              "description": "string",
              "players": [
                {{
                  "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                  "created_at": "2021-02-20T18:50:15.543Z",
                  "modified_at": "2021-02-20T18:50:15.543Z",
                  "deleted_at": "2021-02-20T18:50:15.543Z",
                  "instance": "string",
                  "surname": "string",
                  "forename": "string",
                  "nickname": "string",
                  "pseudonym": "string"
                }}
              ]
            }}
          ],
          "status": "string"
        }}
        </pre>
    - <strong>GET /game/{{game_uuid}} :lock: :</strong><br>
        This request returns the game with the given uuid. The uuid should be given as part of the url.<br>
        The response looks like:
        <pre>
        {{
          "payload": {{
            "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "created_at": "2021-02-20T18:51:00.873Z",
            "modified_at": "2021-02-20T18:51:00.873Z",
            "deleted_at": "2021-02-20T18:51:00.873Z",
            "instance": "string",
            "name": "string",
            "description": "string",
            "players": [
              {{
                "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "created_at": "2021-02-20T18:51:00.873Z",
                "modified_at": "2021-02-20T18:51:00.873Z",
                "deleted_at": "2021-02-20T18:51:00.873Z",
                "instance": "string",
                "surname": "string",
                "forename": "string",
                "nickname": "string",
                "pseudonym": "string"
              }}
            ]
          }},
          "status": "string"
        }}
        </pre>
    - <strong>DELETE /game/{{game_uuid}} :lock: :</strong><br>
        This request deletes the game with the uuid given in the path.
    - <strong>PUT /game/ :lock: :</strong><br>
        This request creates or updates a game. The request body can look like:
        <pre>
        {{
          "name": "string",
          "description": "string",
          "players": [
            "string"
          ],
          "uuid": "string"
        }}
        </pre>
      If no uuid is given, a new game will be created. `players` must be provided or at least an empty array.

    """.format(host=host, hostWithoutScheme=hostWithoutScheme, hostWithoutPort=hostWithoutPort)
    return readme
