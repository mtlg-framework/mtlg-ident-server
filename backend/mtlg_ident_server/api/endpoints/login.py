from fastapi import APIRouter
from mongoengine import DoesNotExist
from backend.mtlg_ident_server.api.schemata.login import ErrorResponse, PseudonymRequest, MethodLoginRequest, PlayerResponse
from backend.mtlg_ident_server.models import Player, Login
from backend.mtlg_ident_server.api.endpoints import return_error

router = APIRouter()

@router.post('/pseudonym/', tags=['login'], response_model=PlayerResponse, responses={400: {"model": ErrorResponse}})
def login_pseudonym(login_data: PseudonymRequest):
    try:
        pseudonym = "".join(login_data.pseudonym.split()) #clear form all whitespaces
        pseudonym = pseudonym.lower()
        player = Player.objects.get(pseudonym__iexact=pseudonym)
        return {
            'pseudonym': player.pseudonym,
            'nickname': player.nickname
        }
    except Exception:
        return return_error(400, "Login failed")

@router.post('/method/', tags=['login'], response_model=PlayerResponse, responses={400: {"model": ErrorResponse}, 500: {"model": ErrorResponse}})
def login_method(login_data: MethodLoginRequest):
    try:
        player = Player.objects.get(nickname=login_data.nickname)
        try:
            login = Login.objects.get(login_method=login_data.method, login_value=login_data.value, player=player)
        except Exception as e:
            return return_error(404, "Wrong Login")

        return {
            'pseudonym': player.pseudonym,
            'nickname': player.nickname
        }
    except DoesNotExist as err:
        return return_error(404, "Player not found")
    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)
