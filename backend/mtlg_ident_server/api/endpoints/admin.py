import datetime
import random
import string

from fastapi import APIRouter, Header, HTTPException, Cookie, Depends
from mongoengine import DoesNotExist
from starlette.responses import Response


from backend.mtlg_ident_server.api.schemata import FrontendResponse
from backend.mtlg_ident_server.api.rredis import redis_connection
from backend.mtlg_ident_server.api.schemata.login import AdminLoginRequest, SuccessResponse, UserResponse
from backend.mtlg_ident_server.models import User
from backend.mtlg_ident_server.api.endpoints import return_error
from backend.mtlg_ident_server.api import get_current_user, auth_required

from backend.settings import config
router = APIRouter()

@router.post("/login/", tags=['admin'] )
def login(login_data: AdminLoginRequest, response: Response):
    username = login_data.username
    password = login_data.password

    user = None

    try:
        user = User.objects.get(username=username)
    except DoesNotExist:
        if username == 'admin' and password == '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4':
            user = User(
                username='admin',
                email='admin@mtlg.de'
            )
            user.set_password('03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4')
            user.save()

        raise HTTPException(status_code=403, detail="error")

    if user.check_password(password):
        response.set_cookie(
            key='loginstatus',
            value=str(datetime.datetime.now()),
            secure=False,
            httponly=False
        )
        # generate new auth token
        token = ''.join(random.choice(string.ascii_letters+string.digits) for _ in range(32))
        redis_connection.set(f"login-{token}-uuid", str(user.uuid))

        response.set_cookie(
            key='token',
            value=token,
            httponly=True
        )
        return {
            "user": user.as_dict(),
            "token": token
        }

    raise HTTPException(status_code=403, detail="error")

@router.delete('/login/', tags=['admin'], response_model=SuccessResponse)
def logout(response: Response):
    response.delete_cookie('token')
    response.delete_cookie('loginstatus')

    return SuccessResponse(success=True)

@router.get('/current_user', tags=['admin'], status_code=200, dependencies=[Depends(auth_required)])
def current_user(current_user: User = Depends(get_current_user)):
    return FrontendResponse[UserResponse](payload=current_user)
