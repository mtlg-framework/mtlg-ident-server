from typing import List
from uuid import UUID

from fastapi import APIRouter
from backend.mtlg_ident_server.api.schemata import FrontendResponse
from backend.mtlg_ident_server.api.schemata.login import ErrorResponse, GameFullResponse, GameResponse, GamesResponse, CreateGameRequest, SuccessResponse
from backend.mtlg_ident_server.models import Game, GameConfig, Player
from backend.mtlg_ident_server.api.endpoints import get_pseudonym, return_error

router = APIRouter()

@router.get("/games/", tags=['game'], response_model=FrontendResponse[List[GameFullResponse]])
def get_games():
    games = Game.objects()
    return FrontendResponse[List[GameFullResponse]](payload=list(games))

@router.get("/game/{game_uuid}", tags=['game'], response_model=FrontendResponse[GameFullResponse], responses={404: {"model": ErrorResponse}})
def get_game(game_uuid: UUID):
    try:
        game = Game.objects.get(uuid=game_uuid)
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(404, "Game not found")

    return FrontendResponse[GameFullResponse](payload=game)

@router.delete("/game/{game_uuid}", tags=['game'], response_model=SuccessResponse, responses={404: {"model": ErrorResponse}})
def delete_game(game_uuid: UUID):
    try:
        game = Game.objects.get(uuid=game_uuid)
        allConfigs = GameConfig.objects()
        try:
            config = GameConfig.objects.get(game=game)
            config.delete()
        except Exception as e:
            print("Exception: {0}".format(e))
        game.delete()
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(404, "Game not found")

    return SuccessResponse(success=True)

@router.put("/game/", tags=['game'], response_model=FrontendResponse[GameFullResponse], responses={404: {"model": ErrorResponse}})
def put_game(game_data: CreateGameRequest):
    try:
        players = []
        for player in game_data.players:
            player_obj = Player.objects.get(pseudonym=player)
            players.append(player_obj)
        if game_data.uuid:
            try:
                game = Game.objects.get(uuid=game_data.uuid)
                game.name = game_data.name
                game.description = game_data.description
                game.players = game_data.players
                game.save()
            except Exception as e:
                game = Game(uuid=game_data.uuid, name=game_data.name, description=game_data.description, players= players)
                game.save()
        else:
            game = Game(name=game_data.name, description=game_data.description, players= players)
            game.save()
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(404, "Player not found")
    return FrontendResponse[GameFullResponse](payload=game)
