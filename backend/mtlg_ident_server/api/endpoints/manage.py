from uuid import UUID
from fastapi import APIRouter, Depends
from typing import List
from mongoengine import DoesNotExist
from backend.mtlg_ident_server.api.schemata import FrontendResponse
from backend.mtlg_ident_server.api.schemata.login import PlayersNameResponse, SuccessResponse, ErrorResponse, PlayerResponse, PlayerCreationResponse, CreatePlayersRequest, DeleteLoginRequest, SetNicknameRequest, PseudonymRequest, CreatePlayerRequest, CreateLoginRequest, ManageLoginMethodRequest
from backend.mtlg_ident_server.models import GameConfig, Player, Login, StandardGameConfig, Game
from backend.mtlg_ident_server.api.endpoints import get_pseudonym, return_error, trim_pseudonym, normalize_pseudonym
from backend.mtlg_ident_server.api import get_current_user, auth_required
router = APIRouter()

@router.get("/players/", tags=['manage'])
def get_players(game_uuid: UUID = None):
    if game_uuid == None:
        config = StandardGameConfig.objects.first()
        if config.listable_names:
            players = Player.objects(nickname__ne="")
            return {
                "players": [p.nickname for p in players]
            }
        else:
            return return_error(405, "Not allowed")
    else:
        try:
            game = Game.objects.get(uuid=game_uuid)
            try:
                config = GameConfig.objects.get(game=game)
            except Exception as e:
                config = StandardGameConfig.objects.first()
            if config.listable_names:
                players = Player.objects(nickname__ne="")
                return {
                    "players": [p.nickname for p in players]
                }
            else:
                return return_error(405, "Not allowed")
        except Exception as e:
            print(e)
            return return_error(405, "Not allowed")


@router.get("/players/full/", tags=['manage'], response_model=FrontendResponse[List[PlayersNameResponse]], dependencies=[Depends(auth_required)])
def get_players_full():
    players = [s for s in Player.objects]
    return FrontendResponse[List[PlayersNameResponse]](payload=players)

@router.post("/players/", tags=['manage'], response_model=PlayerCreationResponse, responses={400: {"model": ErrorResponse}}, dependencies=[Depends(auth_required)])
def create_players(data: CreatePlayersRequest):
    players = []
    for player in data.players:
        if player.nickname != "":
            players_obj = Player.objects(nickname=player.nickname)
            if(players_obj.count() > 0):
                # TODO: rollback when error
                return return_error(400, 'Nickname (' + player.nickname + ') already taken.')
        pseudonym = get_pseudonym(data.language)
        pseudonymSave = trim_pseudonym(pseudonym)
        player_obj = Player(nickname=player.nickname, pseudonym=pseudonymSave, surname=player.surname, forename=player.forename)
        player_obj.save()
        if player.login_method == '':
            players.append(player_obj)
        else:
            login = Login(login_method=player.login_method, login_value=player.login_value, player=player_obj)
            login.save()
            players.append(player_obj)
    return {
        'players': players
    }

@router.get("/players/{pseudonym}", tags=['manage'], response_model=FrontendResponse[PlayersNameResponse], responses={400: {"model": ErrorResponse}}, dependencies=[Depends(auth_required)])
def get_player(pseudonym: str):
    try:
        pseudonym = trim_pseudonym(pseudonym)
        player = Player.objects.get(pseudonym__iexact=pseudonym)
    except Exception as e:
        return return_error(404, 'Player not found')
    return FrontendResponse[PlayersNameResponse](payload=player)

@router.put("/player/", tags=['manage'], response_model=PlayerResponse, responses={400: {"model": ErrorResponse}, 500: {"model": ErrorResponse}})
def create_player(data: CreatePlayerRequest):
    nickname = data.nickname
    # Edit player
    if(data.uuid):
        data.pseudonym = trim_pseudonym(data.pseudonym)

        player = Player.objects.get(uuid=data.uuid)
        player.nickname = data.nickname
        player.forename = data.forename
        player.surname = data.surname
        player.pseudonym = data.pseudonym
        player.save()
        return {
            "nickname": player.nickname,
            "pseudonym": player.pseudonym
        }

    # create player
    players = Player.objects(nickname=nickname)
    if(players.count() > 0):
        return return_error(400, 'Nickname already taken.')
    try:
        pseudonym = get_pseudonym(data.language)
        pseudonymSave = trim_pseudonym(pseudonym)
        player = Player(nickname=nickname, pseudonym=pseudonymSave, surname=data.surname, forename=data.forename)
        player.save()
    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)

    return {
        "nickname": player.nickname,
        "pseudonym": pseudonymSave
    }

@router.delete("/player/{pseudonym}", tags=['manage'], response_model=SuccessResponse, responses={404: {"model": ErrorResponse}, 500: {"model": ErrorResponse}}, dependencies=[Depends(auth_required)])
def delete_player(pseudonym: str):
    try:
        pseudonym = trim_pseudonym(pseudonym)
        player = Player.objects.get(pseudonym__iexact=pseudonym)
        player.delete()
        return {
            "success": True
        }
    except DoesNotExist as err:
        return return_error(404, err)
    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)



@router.put("/login/", tags=['manage'], response_model=PlayerResponse, responses={400: {"model": ErrorResponse}, 500: {"model": ErrorResponse}})
def create_login(login_data: CreateLoginRequest):
    nickname = login_data.nickname
    players = Player.objects(nickname=nickname)

    if(players.count() > 0):
        return return_error(400, 'Nickname already taken.')
    try:
        pseudonym = get_pseudonym(login_data.language)
        pseudonymSave = trim_pseudonym(pseudonym)
        player = Player(nickname=nickname, pseudonym=pseudonymSave, surname=login_data.surname, forename=login_data.forename)
        player.save()

        login_method = LoginMethod.objects.get(uuid=login_data.login_method)

        login = Login(login_method=login_method, login_value=login_data.login_value, player=player)
        login.save()

    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)

    return {
        "nickname": nickname,
        "pseudonym": player.pseudonym
    }

@router.put("/login/method/", tags=['manage'], response_model=SuccessResponse, responses={ 500: {"model": ErrorResponse}})
def manage_login_method(data: ManageLoginMethodRequest):
    try:
        data.pseudonym = trim_pseudonym(data.pseudonym)
        player = Player.objects.get(pseudonym_iexact=data.pseudonym)
        login_method = LoginMethod.objects.get(uuid=data.login_method)

        login = Login.objects(login_method=login_method, player=player)
        if (login.count() > 0):
            login_obj = login.first()
            login_obj.login_value = data.value
            login_obj.save()
        else:
            login = Login(login_method=data.method, login_value=data.value, player=player)
            login.save()

    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)
    return {
        "success": True
    }

@router.delete("/login/method/", tags=['manage'], response_model=SuccessResponse, responses={404: {"model": ErrorResponse}, 500: {"model": ErrorResponse}})
def delete_login_method(data: DeleteLoginRequest):
    try:
        data.pseudonym = trim_pseudonym(data.pseudonym)
        player = Player.objects.get(pseudonym__iexact=data.pseudonym)

        login = Login.objects(login_method=data.method, player=player)
        if (login.count() > 0):
            login_obj = login.first()
            login_obj.delete()
        else:
            return return_error(404, 'Player did not registered this login method')

    except Exception as err:
        print("Exception: {0}".format(err))
        return return_error(500, err)
    return {
        "success": True
    }

@router.put('/setNickname/', tags=['manage'], response_model=PlayerResponse, responses={400: {"model": ErrorResponse}, 404: {"model": ErrorResponse}})
def set_nickname(data: SetNicknameRequest):
    try:
        data.pseudonym = trim_pseudonym(data.pseudonym)
        player = Player.objects.get(pseudonym__iexact=data.pseudonym)
        players = Player.objects(nickname=data.newNick)
        if(players.count() > 0):
            return return_error(400, 'Nickname already taken.')
        player.nickname = data.newNick
        player.save()
        return {
            'pseudonym': player.pseudonym,
            'nickname': player.nickname
        }
    except Exception:
        return return_error(404, "Player does not exist")

@router.get('/normalized/{pseudonym}', tags=['manage'])
def get_normalized_pseudonym(pseudonym: str):
    return normalize_pseudonym(pseudonym)
