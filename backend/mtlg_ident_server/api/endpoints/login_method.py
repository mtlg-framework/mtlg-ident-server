from fastapi import APIRouter
from typing import List
from uuid import UUID

from backend.mtlg_ident_server.api.endpoints import return_error
from backend.mtlg_ident_server.api.schemata.login import LoginMethodRequest, LoginMethodResponse, ErrorResponse, SuccessResponse
from backend.mtlg_ident_server.models import LoginMethod

router = APIRouter()

@router.get('/', tags=['login_method'], responses={500: {"model": ErrorResponse}})
def login_methods():
    try:
        objects = LoginMethod.objects
        return {
            'login_methods': [p.as_dict() for p in objects]
        }
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(500, e)

@router.get('/{uuid}', tags=['login_method'], responses={500: {"model": ErrorResponse}})
def login_methods(uuid: UUID):
    try:
        login_method = LoginMethod.objects.get(uuid=uuid)
        return {
            "payload": login_method.as_dict()
        }
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(500, e)

@router.post('/', tags=['login_method'], response_model=LoginMethodResponse, responses={500: {"model": ErrorResponse}})
def create_login_method(data: LoginMethodRequest):
    try:
        methods = LoginMethod.objects(name=data.name)
        if(methods.count() > 0):
            return return_error(400, 'Login method already created')
        login_method = LoginMethod(name=data.name, description=data.description)
        login_method.save()
        return login_method
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(500, e)

@router.put('/{uuid}', tags=['login_method'], response_model=LoginMethodResponse, responses={500: {"model": ErrorResponse}})
def put_login_method(uuid: UUID, data: LoginMethodRequest):
    try:
        login_method = LoginMethod.objects.get(uuid=uuid)
        login_method.name = data.name
        login_method.description = data.description
        login_method.save()
        print(login_method.as_dict())
        return login_method
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(500, e)

@router.delete('/{uuid}', tags=['login_method'], response_model=SuccessResponse, responses={500: {"model": ErrorResponse}})
def delete_login_method(uuid: UUID):
    try:
        login_method = LoginMethod.objects.get(uuid=uuid)
        login_method.delete()
        return SuccessResponse(success=True)
    except Exception as e:
        print("Exception: {0}".format(e))
        return return_error(500, e)
