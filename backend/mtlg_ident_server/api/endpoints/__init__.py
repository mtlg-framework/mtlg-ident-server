import requests
import json
from fastapi.responses import JSONResponse

from backend.mtlg_ident_server.models import Login, Player

def get_pseudonym(lang='de'):
    pseudonym_request = requests.get("http://mtlg.elearn.rwth-aachen.de/pseudo/pseudonym/" + lang)
    pseudonym_request.encoding = 'utf-8'
    pseudonym_data = json.loads(pseudonym_request.text)
    return pseudonym_data['pseudonym']

def return_error(code, msg):
    return JSONResponse(status_code=code, content={
        "error": msg
    })

def trim_pseudonym(pseudonym):
    pseudonym = "".join(pseudonym.split())

    return pseudonym

def normalize_pseudonym(pseudonym):
    pseudonym = trim_pseudonym(pseudonym)
    pseudonym = pseudonym.lower()
    return pseudonym
