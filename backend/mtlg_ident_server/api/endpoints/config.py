from uuid import UUID
from fastapi import APIRouter, Depends

from backend.mtlg_ident_server.api import auth_required
from backend.mtlg_ident_server.api.schemata import FrontendResponse
from backend.mtlg_ident_server.api.schemata.login import ConfigResponse, CreateConfigRequest, ErrorResponse
from backend.mtlg_ident_server.models import GameConfig, Game, StandardGameConfig
from backend.mtlg_ident_server.api.endpoints import return_error

router = APIRouter()

@router.put("/standard", tags=['config'], response_model=ConfigResponse)
def put_standard_configuration(data: CreateConfigRequest):
    try:
        standard_config = StandardGameConfig.objects.first()
        standard_config.login_methods = data.login_methods
        standard_config.listable_names = data.listable_names
        standard_config.listable_pseudonyms = data.listable_pseudonyms
        standard_config.save()
    except Exception as e:
        return return_error(400, "Config could not be changed")

@router.get("/", tags=['config'], response_model=ConfigResponse)
@router.get("/{game_uuid}", tags=['config'], response_model=ConfigResponse)
def get_configuration(game_uuid: UUID = None):
    try:
        game = Game.objects.get(uuid=game_uuid)
        config = GameConfig.objects.get(game=game)
    except Exception as e:
        return StandardGameConfig.objects.first()
    return config

@router.put("/", tags=['config'], dependencies=[Depends(auth_required)], response_model=ConfigResponse, responses={400: {"model": ErrorResponse}})
def put_configuration(game_uuid: UUID, config_data: CreateConfigRequest):
    try:
        game = Game.objects.get(uuid=game_uuid)
        try:
            config = GameConfig.objects.get(game=game)
            config.login_methods = config_data.login_methods
            config.listable_names = config_data.listable_names
            config.listable_pseudonyms = config_data.listable_pseudonyms
        except Exception as e:
            config = GameConfig(login_methods=config_data.login_methods, listable_names=config_data.listable_names, listable_pseudonyms=config_data.listable_pseudonyms)
            config.game = game
        config.save()
    except Exception as e:
        print(e)
        return return_error(400, "Config could not be created")
    return config
