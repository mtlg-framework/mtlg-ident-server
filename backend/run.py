import mongoengine
import redis

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .mtlg_ident_server.api.api import api_router
from backend.settings import config

from backend.mtlg_ident_server.models import LoginMethod, StandardGameConfig

def create_app():
    app  = FastAPI(title="MTLG Identification Server", root_path="/ident")
    setup_routes(app)
    setup_mongoengine(app)
    setup_redis(app)

    setup_cors(app)

    return app


def setup_routes(app):
    app.include_router(api_router)


def setup_mongoengine(app):
    mongoengine.connect("identserver", host=config.MONGODB_CONNECTION)
    if LoginMethod.objects.first() == None:
        try:
            pin = LoginMethod(name="Pin")
            pin.save()
            symbols = LoginMethod(name="Symbols")
            symbols.save()
        except Exception as e:
            raise
    if StandardGameConfig.objects.first() == None:
        try:
            standard_config = StandardGameConfig(login_methods=["Symbols", "Pin"], listable_names=True, listable_pseudonyms=True)
            standard_config.save()
        except Exception as e:
            raise


def setup_redis(app):
    global _redis_conn
    _redis_conn = redis.Redis(host=config.REDIS_CONNECTION, db=0)

def setup_cors(app):
    origins = [
        "*",
        "http://localhost",
        "http://localhost:8080",
    ]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

app = create_app()
