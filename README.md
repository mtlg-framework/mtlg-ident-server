# Ident-Server
The ident-server is to manage players. 
Players with and without login credentials can be created as well as deleted. 
Logins with credentials or pseudonyms can be verified.
The admin can also configure the games.


## Start
You can use the ident server of LuFG I9 (https://mtlg.elearn.rwth-aachen.de/ident/) 
or if you want you can host you own ident server. To do that clone this repository and start it like following:

```
docker-compose -f docker-compose.complete.yml up --build
```

### Using the Ident Server
In order to send the request from you game to your ident server instead of the global one, you have to specify the url in the settings of you game.
You have to add the following code in `game.config.js`:
```javascript
MTLG.loadOptions({
    ...,
    login: {
        host: "url.to.your/server" // without http
    },
    ...
})
```
The ident server is out-of-the-box on port 8002.

### Managing Ident Server
The frontend for the ident server is out-of-the-box on port 8003. (So if you do not configure something else the you can find the frontend under localhost:8003)
There you can manage games, login methods and players.

## API
On https://mtlg.elearn.rwth-aachen.de/ident/docs you find an interactive overview of the API.

### Manage players and login

- **GET /manage/players/:**
    Optional parameter game_id as UUID. The game_id is used to check the permission. If no game_id is given, the 
    standard config is used for checking permission. Returns all players nicknames as json: 
    ```json
    {
      "players": [
        "Nickname1",
        "Nickname2",
        ...
      ] 
    }
    ```
- **GET /manage/players/full/ :lock: :**
    Returns a json with status and payload. The playload contains an array of all players with all information. You have to be authorized for this request.
    ```json
      {
        "payload": [
          {
            "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "created_at": "2021-02-16T16:23:37.738Z",
            "modified_at": "2021-02-16T16:23:37.738Z",
            "deleted_at": "2021-02-16T16:23:37.738Z",
            "instance": "string",
            "surname": "string",
            "forename": "string",
            "nickname": "string",
            "pseudonym": "string"
          }
        ],
        "status": "string"
      }
    ```
- **GET /manage/players/{pseudonym} :lock: :**
    `{pseudonym}` must be replaced with the pseudonym of the player you want to get. It returns the full inforamtion of the player.
    ```json
        {
          "payload": {
            "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "created_at": "2021-02-16T16:29:26.471Z",
            "modified_at": "2021-02-16T16:29:26.471Z",
            "deleted_at": "2021-02-16T16:29:26.471Z",
            "instance": "string",
            "surname": "string",
            "forename": "string",
            "nickname": "string",
            "pseudonym": "string"
          },
          "status": "string"
        }
    ```
- **POST /manage/players/ :lock: :**
    This request creates several new players. The request body can look like:
    ```json
      {
        "players": [
          {
            "surname": "string", 
            "forename": "string",
            "nickname": "string",
            "login_method": "string",
            "login_value": "string"
          }
        ],
        "language": "string"
      }
    ```
    In the players array you can provide player objects. None parameter in the player object is required but you have to give at least one.  
    The response is like the request body but contains the genereated pseudonym and meta info like uuid and dates.
- **PUT /manage/player/:**
    This request creates or updates a player. The request body can look like:
    ```json
    {
      "nickname": "string",
      "surname": "string",
      "forename": "string",
      "language": "string", // default: "de"
      "uuid": "string",
      "pseudonym": "string"
    }
    ```  
    None of the parameters is required. If you do not provide any of them, a pseudonym is requested and you can set nickname, etc at a later time.
    The response looks like:
    ```json
      {
        "nickname": "string",
        "pseudonym": "string"
      }
    ```
- **PUT /manage/login/:**
    This request also creates a player but with a login method already. The request body can look like:
    ```json
    {
      "nickname": "string",
      "surname": "string",
      "forename": "string",
      "login_method": "string",
      "login_value": "string",
      "language": "string", // default: "de"
    }
    ``` 
    `nickname`, `login_method` and `login_value` are required. `login_method` takes the uuid of an existing login method.
    The successful response looks like:
    ```json
      {
        "nickname": "string",
        "pseudonym": "string"
      }
    ```
- **PUT /manage/login/method/:**
    This request creats or updates a login for the given pseudonym. The request body should look like
    ```json
    {
      "method": "string",
      "value": "string",
      "pseudonym": "string"
    }
    ``` 
    where `method` the uuid of the desired login method is. The json response gives:
    ```json
    {
      "success": true
    }
    ```
- **PUT /manage/setNickname/:**
    This request sets the nickname of the given pseudonym. The request body must look like:
    ```json
    {
      "pseudonym": "string",
      "newNick": "string"
    }
    ```
    The successful response looks like:
    ```json
    {
      "nickname": "string",
      "pseudonym": "string"
    }
    ```
- **DELETE /manage/player/{pseudonym} :lock: :**
    This request deletes the player with the given pseudonym. The json response looks like:
    ```json
    {
      "success": true
    }
    ```
- **DELETE /manage/login/method/:**
    This request deletes the login method of the player with the given pseudonym. 
    The request body should look like:
    ```json
    {
      "method": "string",
      "pseudonym": "string"
    }
    ```
    `method` takes the uuid id of the login method that should be deleted.
    The json response looks like:
    ```json
    {
      "success": true
    }
    ```

### Admin 
- **POST /admin/login/**
    This request logs in the admin user if authorized. It takes the username and password
- **DELETE /admin/login/**
    This request logs out the logged in user.
- **GET /admin/current_user :lock:**
    Returns the currently logged in user. Only possible when authorized.

### Login
- **POST /login/pseudonym/:**
    This request logs in a player with their pseudonym. And return nickname and pseudonym.
- **POST /login/method/:**
    This request logs in a player with the given nickname, login method and value if correct.

### Creating Login methods (the methods not the credentials themselves)
- **GET /login_method/:**
    This request returns all created login methods like:
    ```json
    {
      "login_methods": [
          {
            "_instance": "login_method",
            "name": "Pin",
            "description": "string",
            "class": "string",
            "created_at": "2020-10-26 09:26:29.321000",
            "modified_at": "2020-10-26 13:11:29.903000",
            "deleted_at": "None",
            "uuid": "99b53d9c-4382-4bec-b79f-d24f1f4cee0c",
            "_id": "5f96964537870d293f88c14f"
          }
      ]  
    }   
    ```
- **POST /login_method/:**
    Creates a login method. The request body should look like:
    ```json
    {
      "name": "string",
      "description": "string"
    }
    ```
    Response looks like:
    ```json
    {
      "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "created_at": "2021-02-20T18:27:53.901Z",
      "modified_at": "2021-02-20T18:27:53.901Z",
      "deleted_at": "2021-02-20T18:27:53.901Z",
      "instance": "string",
      "name": "string",
      "description": "string"
    }
    ```
- **GET /login_method/{uuid}:**
    This request returns the specifed login method. The response looks like:
    ```json
    {
      "payload": {
        "_instance": "login_method",
        "name": "Pin",
        "description": "string",
        "class": "string",
        "created_at": "2020-10-26 09:26:29.321000",
        "modified_at": "2020-10-26 13:11:29.903000",
        "deleted_at": "None",
        "uuid": "99b53d9c-4382-4bec-b79f-d24f1f4cee0c",
        "_id": "5f96964537870d293f88c14f"
      }
    }
    ``` 
- **PUT /login_method/{uuid}:**
    This method updates the login method with the given uuid. The request body should look like:
    ```json
    {
    "name": "string",
    "description": "string"
    }
    ```
    Response looks like:
    ```json
    {
    "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
    "created_at": "2021-02-20T18:27:53.901Z",
    "modified_at": "2021-02-20T18:27:53.901Z",
    "deleted_at": "2021-02-20T18:27:53.901Z",
    "instance": "string",
    "name": "string",
    "description": "string"
    }
    ```
- **DELETE /login_method/{uuid}:**
    This request deletes the login method with the given uuid.
    The json response looks like:
    ```json
    {
      "success": true
    }
    ```

### Configuration of games
- **PUT /config/standard:**
    This request creates or updates the standard config. The standard config is used for games where no config has been set.
    The request body must look like:
    ```json
    {
      "login_methods": [
        "string"
      ],
      "listable_names": true,
      "listable_pseudonyms": true
    }
    ```
    Here `login_methods` is an array of the names of the methods. `listable_names` indicates if the names of the players can be shown as list in the game. Same for `listable_pseudonyms`.
- **GET /config/{game_uuid}:**
    This request returns the config of the given uuid of a game. Here the game uuid is a paremeter in the url.
    The response looks like:
    ```json
    {
      "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "created_at": "2021-02-20T18:47:21.298Z",
      "modified_at": "2021-02-20T18:47:21.298Z",
      "deleted_at": "2021-02-20T18:47:21.298Z",
      "instance": "string",
      "login_methods": [
        "string"
      ],
      "listable_names": true,
      "listable_pseudonyms": true
    }
    ```
- **GET /config/:**
    Same as **GET /config/{game_uuid}:** but here the uuid of the game is given as query parameter

- **PUT /config/ :lock: :**
    This request creats or updates the configuration for a given game.
    The uuid of the game must be given as query parameter and the request body must look like:
    ```json
    {
      "login_methods": [
        "string"
      ],
      "listable_names": true,
      "listable_pseudonyms": true
    }
    ```
    The response looks like:
    ```json
    {
      "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
      "created_at": "2021-02-20T18:49:38.338Z",
      "modified_at": "2021-02-20T18:49:38.338Z",
      "deleted_at": "2021-02-20T18:49:38.338Z",
      "instance": "string",
      "login_methods": [
        "string"
      ],
      "listable_names": true,
      "listable_pseudonyms": true
    }
    ```

### Games
- **GET /games/ :lock: :**
    This request returns all created games. The response looks like:
    ```json
    {
      "payload": [
        {
          "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
          "created_at": "2021-02-20T18:50:15.543Z",
          "modified_at": "2021-02-20T18:50:15.543Z",
          "deleted_at": "2021-02-20T18:50:15.543Z",
          "instance": "string",
          "name": "string",
          "description": "string",
          "players": [
            {
              "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
              "created_at": "2021-02-20T18:50:15.543Z",
              "modified_at": "2021-02-20T18:50:15.543Z",
              "deleted_at": "2021-02-20T18:50:15.543Z",
              "instance": "string",
              "surname": "string",
              "forename": "string",
              "nickname": "string",
              "pseudonym": "string"
            }
          ]
        }
      ],
      "status": "string"
    }
    ```
- **GET /game/{game_uuid} :lock: :**
    This request returns the game with the given uuid. The uuid should be given as part of the url.
    The response looks like:
    ```json
    {
      "payload": {
        "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
        "created_at": "2021-02-20T18:51:00.873Z",
        "modified_at": "2021-02-20T18:51:00.873Z",
        "deleted_at": "2021-02-20T18:51:00.873Z",
        "instance": "string",
        "name": "string",
        "description": "string",
        "players": [
          {
            "uuid": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
            "created_at": "2021-02-20T18:51:00.873Z",
            "modified_at": "2021-02-20T18:51:00.873Z",
            "deleted_at": "2021-02-20T18:51:00.873Z",
            "instance": "string",
            "surname": "string",
            "forename": "string",
            "nickname": "string",
            "pseudonym": "string"
          }
        ]
      },
      "status": "string"
    }
    ```
- **DELETE /game/{game_uuid} :lock: :**
    This request deletes the game with the uuid given in the path.
- **PUT /game/ :lock: :**
    This request creates or updates a game. The request body can look like:
    ```json
    {
      "name": "string",
      "description": "string",
      "players": [
        "string"
      ],
      "uuid": "string"
    }
    ```
  If no uuid is given, a new game will be created. `players` must be provided or at least an empty array.
